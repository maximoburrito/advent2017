(ns advent2017.day20
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

;; coming back to advent2017 in 2023 and doing this is strange
;; the problem specifications have come such a long way over the
;; years. this one is pretty bad, looking back on it.

(defn parse-part [part]
  (let [[_ c x y z] (re-matches #"(\w+)=<(-?\d+),(-?\d+),(-?\d+)>" part)]
    [(keyword c) [(parse-long x)
                  (parse-long y)
                  (parse-long z)]]))


(defn indexed [xs]
  (map-indexed vector xs))

(defn parse-input [text]
  (for [[n line] (indexed (str/split-lines text))]
    (into {:n n}
          (for [part (str/split line #", ")]
            (parse-part part)))))

(def full-input (parse-input (slurp (io/resource "day20-input.txt"))))


(defn manh [[x y z]]
  (+ (abs x)
     (abs y)
     (abs z)))



(defn part1 [input]
  ;; I assume this is correct - order by best a then best v then best p
  ;; it seemed logical to assume that the highest order dominates, and it worked
  (let [[_ _ _ n]
        (first
          (sort
            (for [{:keys [n p v a]} input]
              [(manh a) (manh v) (manh p) n])))]
    n))


(defn add [[x y z] [dx dy dz]]
  [(+ x dx)
   (+ y dy)
   (+ z dz)])


(defn step [{:keys [n p v a]}]
  ;; the position calculation is dumb. I actually
  ;; coded it like this the first time, more elegantly than this,
  ;; but convinced I misread it I coded it as (add p v) and then
  ;; it morphed into this.
  {:n n
   :p (add (add p v) a)
   :v (add v a)
   :a a})

(defn part2 [input]
  ;; I don't know what the "correct" solution here. I ran this until I felt the
  ;; numbers wouldn't go down more and then hardcoded a number well above it
  ;; for the version we see here.
  (loop [particles input
         n 0]
    (if (< n 1000)
      (let [new-particles (map step particles)
            pcounts (frequencies (map :p new-particles))]
        (recur
          (for [particle new-particles
                :when (= 1 (pcounts (:p particle)))]
            particle)
          (inc n)))
      (count particles))))

(comment
  (part1 full-input)
  243

  (part2 full-input)
  648)
