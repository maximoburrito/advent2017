(ns advent.day19
  (:require [clojure.java.io :as io]
            [clojure.string :as str]))

(defn indexed [xs]
  (map-indexed vector xs))

(defn parse-input [text]
  (into {}
        (for [[y line] (indexed (str/split-lines text))
              [x c] (indexed line)
              :when (not= \space c)]
          [[x y] c])))

(def full-input (parse-input (slurp (io/resource "day19-input.txt"))))
(def sample-input (parse-input (slurp (io/resource "day19-sample1.txt"))))


(def dirs {:down  [0 1]
           :up    [0 -1]
           :left  [-1 0]
           :right [1 0]})

(defn add [[x y] [dx dy]]
  [(+ x dx) (+ y dy)])

(defn move [pos dir]
  (add pos (dirs dir)))

(defn start-pos [input]
  (first
    (filter (fn [[x y]] (= y 0))
            (keys input))))

(defn turn [input pos dir]
  (if (or (= :left dir) (= :right dir))
    (if (input (move pos :up))   :up   :down)
    (if (input (move pos :left)) :left :right)))

(defn route [input]
  (loop [pos (start-pos input)
         dir :down
         path []]

    (let [c (input pos)]
      (cond
        (nil? c)
        path

        (<= (int \A) (int c) (int \Z))
        (recur
          (move pos dir)
          dir
          (conj path c))


        (= \+ c)
        (let [turn-dir (turn input pos dir)]
          (recur
            (move pos turn-dir)
            turn-dir
            (conj path turn-dir)))

        :else
        (recur
          (move pos dir)
          dir
          (conj path dir))))))


(defn part1 [input]
  (apply str
         (filter char? (route input))))

(defn part2 [input]
  (count (route input)))

;;



(comment
  (part1 sample-input)
  "ABCDEF"
  (part1 full-input)
  "PVBSCMEQHY"

  (part2 sample-input)
  38
  (part2 full-input)
  17736)
